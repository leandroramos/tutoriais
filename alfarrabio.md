#Anotações diversas

##MySQL
- Criar usuário
    - `CREATE USER 'usuario'@'%' IDENTIFIED BY 'senha';`
- Dar acesso total ao usuário a um banco específico
    - `GRANT ALL PRIVILEGES ON banco.* TO 'usuario'@'localhost';`
- Alterar a senha do usuário
    - `ALTER USER 'usuario'@'localhost' IDENTIFIED WITH mysql_native_password BY 'senha';`

##Chaves de repositórios
- Invalid key:
    - apt-key adv --recv-keys --keyserver pgp.mit.edu <numero-da-chave>
