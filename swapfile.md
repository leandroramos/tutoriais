# Criando swapfile no GNU/Linux

**Importante:** se você usa login de root, não vai precisar do **sudo** nos comandos abaixo. Como sempre, adapte o tutorial às suas necessidades.

- Abra um terminal qualquer.

- Crie um arquivo de 2GB (ou do tamanho que você quiser)

    - `sudo fallocate -l 2G /swapfile`

- Ajuste a permissão do arquivo, para que apenas o root possa ler e escrever nele

    - `sudo chmod 600 /swapfile`

- Formate o arquivo como swap

    - `sudo mkswap /swapfile`

- Ligue o swap

    - `sudo swapon /swapfile`

- Coloque o arquivo de swap no FSTAB para que ele monte na inicialização

    - `sudo nano /etc/fstab`

    - Na última linha do arquivo, adicione a linha abaixo

        - `/swapfile none swap sw 0 0`

    - Salve o arquivo




