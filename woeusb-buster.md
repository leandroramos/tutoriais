#WoeUSB no Debian Buster

A versão atual do programa (05/12/2020) não funciona no Debian Buster, mas podemos instalar uma versão mais antiga através do python3-pip.

## Dependências
```
sudo apt install git p7zip-full python3-pip python3-wxgtk4.0
```

## Instalação do programa
```
sudo pip3 install WoeUSB-ng==0.2.3
```
